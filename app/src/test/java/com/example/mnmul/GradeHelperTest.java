package com.example.mnmul;

import com.example.mnmul.ikpmd_applicatie.Helpers.GradeHelper;
import com.example.mnmul.ikpmd_applicatie.Models.Grade;
import com.example.mnmul.ikpmd_applicatie.Models.Subject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GradeHelperTest {

    private List<Grade> grades;

    @Before
    public void setUp() {
        grades = new ArrayList<>();
    }

    public void addGrade() {
        Subject subject = new Subject(1, "ITST", "Testen van software", 3);
        Grade grade = new Grade(1, subject.getKey(), "5.8");
        grade.setSubject(subject);
        grades.add(grade);
    }

    public void addGradeWithoutSubject() {
        grades.add(new Grade(1, "random", "5.8"));
    }

    public void addInsufficientGrade() {
        Subject subject = new Subject(1, "ISECU", "IT security", 3);
        Grade grade = new Grade(1, subject.getKey(), "4.5");
        grade.setSubject(subject);
        grades.add(grade);
    }

    @Test
    public void testGetAverageGradeWithStringValueReturnsZero() {
        Grade grade = new Grade(1, "1", "V");
        grades.add(grade);
        double expected = 0.0;

        double result = GradeHelper.getAverageGrade(grades);

        Assert.assertEquals(expected, result, 0.0);
    }

    @Test
    public void testGetAverageGradeOneValueReturnsCorrectAverage() {
        Grade grade = new Grade(1, "1", "5.5");
        grades.add(grade);
        double expected = 5.5;

        double result = GradeHelper.getAverageGrade(grades);

        Assert.assertEquals(expected, result, 0.0);
    }

    @Test
    public void testGetAverageGradeMultipleValuesReturnsCorrectAverage() {
        Grade grade = new Grade(1, "1", "5.8");
        Grade grade2 = new Grade(1, "1", "7.2");
        Grade grade3 = new Grade(1, "1", "9.3");
        grades.add(grade);
        grades.add(grade2);
        grades.add(grade3);
        double expected = 7.43;

        double result = GradeHelper.getAverageGrade(grades);

        Assert.assertEquals(expected, result, 0.0);
    }

    @Test
    public void testGetAchievedPointsWithoutSubjectReturnsZero() {
        addGradeWithoutSubject();
        int expected = 0;

        int result = GradeHelper.getAchievedPoints(grades);

        Assert.assertEquals(expected, result, 0.0);
    }

    @Test
    public void testGetAchievedPointsWithInsufficientGradeReturnsCorrectAmount() {
        addGrade();
        addInsufficientGrade();
        int expected = 3;

        int result = GradeHelper.getAchievedPoints(grades);

        Assert.assertEquals(expected, result, 0.0);
    }

    @Test
    public void testGetAchievedPointsReturnsCorrectAmount() {
        addGrade();
        addGrade();
        int expected = 6;

        int result = GradeHelper.getAchievedPoints(grades);

        Assert.assertEquals(expected, result, 0.0);
    }

}