package com.example.mnmul.ikpmd_applicatie.DataMappers;

import com.example.mnmul.ikpmd_applicatie.Listeners.Listener;
import com.google.firebase.database.DatabaseReference;

public interface UserDataReadable<Key, Type> {

    DatabaseReference getReference(String userId);

    void getAll(String userId, int year, Listener<Type> listener);

    void find(String userId, Key id, Listener<Type> listener);

}