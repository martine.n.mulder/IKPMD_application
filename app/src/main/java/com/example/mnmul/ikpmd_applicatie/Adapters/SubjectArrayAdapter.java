package com.example.mnmul.ikpmd_applicatie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.example.mnmul.ikpmd_applicatie.Models.Subject;
import com.example.mnmul.ikpmd_applicatie.R;
import java.util.List;

public class SubjectArrayAdapter extends ArrayAdapter<Subject> {

    public SubjectArrayAdapter(Context context, int resource, List<Subject> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.view_subject_row, parent, false);
            viewHolder.subjectCode = convertView.findViewById(R.id.subject_code);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Subject subject = getItem(position);
        viewHolder.subjectCode.setText(subject.getCode());

        return convertView;
    }

    private static class ViewHolder {
        public TextView subjectCode;
    }

}