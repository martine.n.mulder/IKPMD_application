package com.example.mnmul.ikpmd_applicatie.Helpers;

import android.util.Log;
import com.example.mnmul.ikpmd_applicatie.Models.Grade;
import java.util.List;

public class GradeHelper {

    public static double getAverageGrade(List<Grade> grades) {
        int validScoreCount = 0;
        double totalScore = 0.0;

        for (Grade grade : grades) {
            try {
                double score = Double.parseDouble(grade.getScore());
                totalScore += score;
                validScoreCount++;
            } catch (NumberFormatException e) {
                Log.i("Allowed exception", "Encountered a string value as score, this is allowed. " + e.getMessage());
            }
        }

        double averageScore = totalScore / validScoreCount;

        return Math.round(averageScore * 100) / 100.00;
    }

    public static int getAchievedPoints(List<Grade> grades) {
        int totalPoints = 0;

        for (Grade grade : grades) {
            if (grade.isSufficient() && grade.getSubject() != null) {
                totalPoints += grade.getSubject().getEc();
            }
        }

        return totalPoints;
    }

}