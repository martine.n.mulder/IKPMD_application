package com.example.mnmul.ikpmd_applicatie.Models;

import com.google.firebase.database.Exclude;

public class Subject {

    @Exclude
    private String key;
    private int year;
    private String code;
    private String title;
    private int ec;

    public Subject() { }

    public Subject(int year, String code, String title, int ec) {
        this.year = year;
        this.code = code;
        this.title = title;
        this.ec = ec;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEc() {
        return ec;
    }

    public void setEc(int ec) {
        this.ec = ec;
    }

    @Override
    public String toString() {
        return code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Subject other = (Subject) obj;
        if (!key.equals(other.key))
            return false;
        return true;
    }

}