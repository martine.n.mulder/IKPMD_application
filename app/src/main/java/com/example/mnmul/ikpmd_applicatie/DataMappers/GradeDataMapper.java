package com.example.mnmul.ikpmd_applicatie.DataMappers;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.mnmul.ikpmd_applicatie.Listeners.Listener;
import com.example.mnmul.ikpmd_applicatie.Models.Grade;
import com.example.mnmul.ikpmd_applicatie.Models.Subject;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GradeDataMapper implements UserDataWritable<Grade>, UserDataReadable<String, Grade> {

    private FirebaseDatabase database;
    private SubjectDataMapper subjectDataMapper;

    public GradeDataMapper(FirebaseDatabase database, SubjectDataMapper subjectDataMapper) {
        this.database = database;
        this.subjectDataMapper = subjectDataMapper;
    }

    @Override
    public void insert(Grade grade, String userId, OnSuccessListener successListener, OnFailureListener failureListener) {
        Log.i("Inserting object", "Inserting grade " + grade.getScore());
        getReference(userId).push().setValue(grade)
        .addOnSuccessListener(successListener)
        .addOnFailureListener(failureListener);
    }

    @Override
    public void update(Grade grade, String userId, OnCompleteListener successListener, OnFailureListener failureListener) {
        Log.i("Updating object", "Updating grade " + grade.getScore());
        getReference(userId)
        .child(grade.getKey())
        .setValue(grade)
        .addOnCompleteListener(successListener)
        .addOnFailureListener(failureListener);
    }

    @Override
    public void delete(Grade grade, String userId, OnSuccessListener successListener, OnFailureListener failureListener) {
        Log.i("Deleting object", "Deleting grade " + grade.getScore());
        getReference(userId)
        .child(grade.getKey())
        .removeValue()
        .addOnSuccessListener(successListener)
        .addOnFailureListener(failureListener);
    }

    public DatabaseReference getReference(String userId) {
        return database.getReference().child("users/" + userId + "/grades");
    }

    public void find(String userId, String id, Listener<Grade> listener) {
        if (id == null) return;
        getReference(userId)
        .child(id)
        .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Grade grade = snapshot.getValue(Grade.class);

                if (grade != null) {
                    grade.setKey(snapshot.getKey());

                    subjectDataMapper.find(grade.getSubjectId(), new Listener<Subject>() {
                        @Override
                        public void onObjectAdded(Subject object) {
                            grade.setSubject(object);
                            listener.onObjectAdded(grade);
                        }

                        @Override
                        public void onObjectChanged(Subject object) {
                            grade.setSubject(object);
                            listener.onObjectChanged(grade);
                        }

                        @Override
                        public void onObjectDeleted(Subject object) {
                            grade.setSubject(object);
                            listener.onObjectDeleted(grade);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("Database error", "" + error.getMessage());
            }
        });
    }

    public void getAll(String userId, int year, Listener<Grade> listener) {
        getReference(userId)
        .orderByChild("year")
        .equalTo(year)
        .addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Grade grade = snapshot.getValue(Grade.class);
                grade.setKey(snapshot.getKey());

                subjectDataMapper.find(grade.getSubjectId(), new Listener<Subject>() {
                    @Override
                    public void onObjectAdded(Subject object) {
                        grade.setSubject(object);
                        listener.onObjectAdded(grade);
                    }

                    @Override
                    public void onObjectChanged(Subject object) {
                        grade.setSubject(object);
                        listener.onObjectChanged(grade);
                    }

                    @Override
                    public void onObjectDeleted(Subject object) {
                        grade.setSubject(object);
                        listener.onObjectDeleted(grade);
                    }

                });
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Grade grade = snapshot.getValue(Grade.class);
                grade.setKey(snapshot.getKey());

                subjectDataMapper.find(grade.getSubjectId(), new Listener<Subject>() {
                    @Override
                    public void onObjectAdded(Subject object) {
                        grade.setSubject(object);
                        listener.onObjectAdded(grade);
                    }

                    @Override
                    public void onObjectChanged(Subject object) {
                        grade.setSubject(object);
                        listener.onObjectChanged(grade);
                    }

                    @Override
                    public void onObjectDeleted(Subject object) {
                        grade.setSubject(object);
                        listener.onObjectDeleted(grade);
                    }
                });
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                Grade grade = snapshot.getValue(Grade.class);
                grade.setKey(snapshot.getKey());
                listener.onObjectDeleted(grade);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("Database error", "" + error.getMessage());
            }
        });
    }

}