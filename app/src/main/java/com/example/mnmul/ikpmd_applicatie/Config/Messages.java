package com.example.mnmul.ikpmd_applicatie.Config;

public class Messages {

    public static String getGradeAddedSuccessMessage(String score, String subjectCode) {
        return "Cijfer \"" + score + "\" toegevoegd voor " + subjectCode;
    }

    public static String getGradeAddedFailureMessage() {
        return "Cijfer toevoegen mislukt";
    }

    public static String getGradeEditedSuccessMessage(String subjectCode) {
        return "Cijfer aangepast voor " + subjectCode;
    }

    public static String getGradeDeletedSuccessMessage() {
        return "Cijfer verwijderd";
    }

    public static String getGradeDeletedFailureMessage() {
        return "Cijfer verwijderen mislukt";
    }

    public static String getGradeEditedFailureMessage() {
        return "Cijfer aanpassen mislukt";
    }

}
