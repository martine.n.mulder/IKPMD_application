package com.example.mnmul.ikpmd_applicatie;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.mnmul.ikpmd_applicatie.Helpers.PreferenceHelper;

public class YearActivity extends BaseActivity {

    private int year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year);

        year = getIntent().getIntExtra("selected_year", -1);

        setUpUi();
    }

    public void setUpUi() {
        setTitle();
    }

    public void setTitle() {
        TextView titleView = findViewById(R.id.text_title);
        String title = year == 3 ? ("Jaar " + year + "/" + (year + 1)) : ("Jaar " + year);
        titleView.setText(title);
    }

    public void startInitialActivity(View view) {
        resetPreferredYear();
        Intent intent = new Intent(YearActivity.this, StartActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("selected_year", -1);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void resetPreferredYear() {
        SharedPreferences preferences = getSharedPreferences("preferred_year", MODE_PRIVATE);
        PreferenceHelper.reset(preferences, "preferred_year");
    }

    public void handleStartGradeActivity(View view) {
        startGradeActivity("");
    }

    public void startGradeActivity(String gradeId) {
        Intent intent = new Intent(this.getApplicationContext(), GradeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("selected_year", year);
        bundle.putString("selected_grade_id", gradeId);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}