package com.example.mnmul.ikpmd_applicatie.Helpers;

import android.content.SharedPreferences;
import android.util.Log;

public class PreferenceHelper {

    public static void store(SharedPreferences preferences, String id, int value) {
        Log.d("Set " + id, "" + value);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(id, value);
        editor.apply();
    }

    public static void reset(SharedPreferences preferences, String id) {
        Log.d("Reset " + id, "-1");
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(id, -1);
        editor.apply();
    }

}
