package com.example.mnmul.ikpmd_applicatie.DataMappers;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;

public interface UserDataWritable<Type> {

    void insert(Type object, String userId, OnSuccessListener onSuccesListener, OnFailureListener failureListener);

    void update(Type object, String userId, OnCompleteListener onSuccesListener, OnFailureListener failureListener);

    void delete(Type object, String userId, OnSuccessListener onSuccesListener, OnFailureListener failureListener);

    DatabaseReference getReference(String userId);

}