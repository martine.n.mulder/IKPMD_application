package com.example.mnmul.ikpmd_applicatie.Config;

public final class Settings {

    public static final int REQUIRED_EC_PROPEDEUSE = 60;
    public static final int REQUIRED_EC_BACHELOR = 180;

}