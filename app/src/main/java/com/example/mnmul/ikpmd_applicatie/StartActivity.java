package com.example.mnmul.ikpmd_applicatie;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.mnmul.ikpmd_applicatie.Helpers.PreferenceHelper;
import com.google.firebase.database.FirebaseDatabase;

public class StartActivity extends BaseActivity {

    private FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        database = FirebaseDatabase.getInstance();
        configureDatabase();

        if (getPreferredYear() != -1) {
            Log.i("Shared preferences", "" + getPreferredYear());
            selectYear(getPreferredYear());
        }
    }

    public void configureDatabase() {
        if (database == null) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
    }

    public int getPreferredYear() {
        SharedPreferences preferences = getSharedPreferences("preferred_year", MODE_PRIVATE);
        return preferences.getInt("preferred_year", -1);
    }

    public void selectYear(int year) {
        storePreferredYear(year);
        startYearActivity(year);
    }

    public void selectYear(View view) {
        int year = Integer.parseInt(view.getTag().toString());
        storePreferredYear(year);
        startYearActivity(year);
    }

    public void startYearActivity(int year) {
        Intent intent = new Intent(StartActivity.this, YearActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("selected_year", year);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void storePreferredYear(int year) {
        SharedPreferences preferences = getSharedPreferences("preferred_year", MODE_PRIVATE);
        PreferenceHelper.store(preferences, "preferred_year", year);
    }

}