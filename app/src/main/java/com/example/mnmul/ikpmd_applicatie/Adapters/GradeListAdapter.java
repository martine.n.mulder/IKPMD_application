package com.example.mnmul.ikpmd_applicatie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.example.mnmul.ikpmd_applicatie.Models.Grade;
import com.example.mnmul.ikpmd_applicatie.R;

import java.util.List;

public class GradeListAdapter extends ArrayAdapter<Grade> {

    public GradeListAdapter(Context context, int resource, List<Grade> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.view_grade_row, parent, false);
            viewHolder.subjectCode = convertView.findViewById(R.id.subject_code);
            viewHolder.subjectPoints = convertView.findViewById(R.id.subject_points);
            viewHolder.score = convertView.findViewById(R.id.grade_score);
            viewHolder.gradeStroke = convertView.findViewById(R.id.grade_stroke);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 1) {
            convertView.setBackgroundColor(ContextCompat.getColor(this.getContext(), R.color.colorAccent3));
        }

        Grade grade = getItem(position);

        if (grade.getSubject() != null) {
            viewHolder.subjectCode.setText(String.valueOf(grade.getSubject().getCode()));
            viewHolder.subjectPoints.setText(String.valueOf(grade.getSubject().getEc()));
        }

        viewHolder.score.setText(String.valueOf(grade.getScore()));
        setGradeStrokeColor(viewHolder.gradeStroke, grade);

        return convertView;
    }

    private void setGradeStrokeColor(LinearLayout gradeStroke, Grade grade) {
        if (!grade.isSufficient()) {
            gradeStroke.setBackgroundColor(ContextCompat.getColor(this.getContext(), R.color.colorPrimary2));
        }
    }

    private static class ViewHolder {
        TextView subjectCode;
        TextView subjectPoints;
        TextView score;
        LinearLayout gradeStroke;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}