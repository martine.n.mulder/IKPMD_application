package com.example.mnmul.ikpmd_applicatie.Models;

import com.google.firebase.database.Exclude;

public class Grade {

    @Exclude
    private String key;
    private int year;
    private String subjectId;
    private String score;
    @Exclude
    private Subject subject;

    public Grade() { }

    public Grade(int year, String subjectId, String score) {
        this.year = year;
        this.subjectId = subjectId;
        this.score = score;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isSufficient() {
        boolean isSufficient = false;

        try {
            if (Double.parseDouble(score) >= 5.5) {
                isSufficient = true;
            }
        } catch (NumberFormatException e) {
            if (score.equals("V")) {
                isSufficient = true;
            }
        }

        return isSufficient;
    }

    @Override
    public String toString() {
        return score;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Grade other = (Grade) obj;
        if (!key.equals(other.key))
            return false;
        return true;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}