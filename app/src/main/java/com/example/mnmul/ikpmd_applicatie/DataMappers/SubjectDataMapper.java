package com.example.mnmul.ikpmd_applicatie.DataMappers;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.mnmul.ikpmd_applicatie.Listeners.Listener;
import com.example.mnmul.ikpmd_applicatie.Models.Subject;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SubjectDataMapper implements DataReadable<String, Subject> {

    private FirebaseDatabase database;

    public SubjectDataMapper(FirebaseDatabase database) {
        this.database = database;
    }

    public DatabaseReference getReference() {
        return database.getReference().child("subjects");
    }

    public void find(String id, Listener<Subject> listener) {
        if (id == null) return;
        getReference()
        .child(id)
        .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Subject subject = snapshot.getValue(Subject.class);

                if (subject != null) {
                    subject.setKey(snapshot.getKey());
                    listener.onObjectAdded(subject);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("Database error", "" + error.getMessage());
            }
        });
    }

    public void getAll(int year, Listener<Subject> listener) {
        getReference()
        .orderByChild("year")
        .equalTo(year)
        .addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Subject subject = snapshot.getValue(Subject.class);
                subject.setKey(snapshot.getKey());
                listener.onObjectAdded(subject);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Subject subject = snapshot.getValue(Subject.class);
                subject.setKey(snapshot.getKey());
                listener.onObjectChanged(subject);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                Subject subject = snapshot.getValue(Subject.class);
                subject.setKey(snapshot.getKey());
                listener.onObjectDeleted(subject);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("Database error", "" + error.getMessage());
            }
        });
    }

}