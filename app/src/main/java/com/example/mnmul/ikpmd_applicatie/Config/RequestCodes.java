package com.example.mnmul.ikpmd_applicatie.Config;

public final class RequestCodes {

    public static final int RC_SIGN_IN = 1;
    public static final int RC_SIGN_OUT = 2;

}