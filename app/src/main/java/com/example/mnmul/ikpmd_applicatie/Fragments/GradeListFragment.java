package com.example.mnmul.ikpmd_applicatie.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.fragment.app.ListFragment;

import com.example.mnmul.ikpmd_applicatie.Adapters.GradeListAdapter;
import com.example.mnmul.ikpmd_applicatie.DataMappers.GradeDataMapper;
import com.example.mnmul.ikpmd_applicatie.DataMappers.SubjectDataMapper;
import com.example.mnmul.ikpmd_applicatie.GradeActivity;
import com.example.mnmul.ikpmd_applicatie.Listeners.Listener;
import com.example.mnmul.ikpmd_applicatie.Models.Grade;
import com.example.mnmul.ikpmd_applicatie.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class GradeListFragment extends ListFragment implements AdapterView.OnItemClickListener {

    private FirebaseAuth mAuth;
    private GradeDataMapper gradeDataMapper;
    private SubjectDataMapper subjectDataMapper;
    private List<Grade> grades = new ArrayList<>();
    private int year;

    public GradeListFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        subjectDataMapper = new SubjectDataMapper(database);
        gradeDataMapper = new GradeDataMapper(database, subjectDataMapper);
        year = getActivity().getIntent().getIntExtra("selected_year", -1);
        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getUid() != null) {
            fetchGrades();
        }

        return inflater.inflate(R.layout.grade_list_fragment, container, false);
    }

    public void fetchGrades() {
        gradeDataMapper.getAll(mAuth.getUid(), year, new Listener<Grade>() {
            @Override
            public void onObjectAdded(Grade grade) {
                grades.add(grade);
                setGradeList();
            }

            @Override
            public void onObjectChanged(Grade grade) {
                grades.set(grades.indexOf(grade), grade);
                setGradeList();
            }

            @Override
            public void onObjectDeleted(Grade grade) {
                grades.remove(grade);
                setGradeList();
            }
        });
    }

    public void setGradeList() {
        GradeListAdapter adapter = new GradeListAdapter(getActivity(), 0, grades);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startGradeActivity(grades.get(position).getKey());
    }

    public void startGradeActivity(String gradeId) {
        Intent intent = new Intent(getActivity(), GradeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("selected_year", year);
        bundle.putString("selected_grade_id", gradeId);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}