package com.example.mnmul.ikpmd_applicatie.Fragments;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.mnmul.ikpmd_applicatie.Config.Settings;
import com.example.mnmul.ikpmd_applicatie.DataMappers.GradeDataMapper;
import com.example.mnmul.ikpmd_applicatie.DataMappers.SubjectDataMapper;
import com.example.mnmul.ikpmd_applicatie.Helpers.GradeHelper;
import com.example.mnmul.ikpmd_applicatie.Listeners.Listener;
import com.example.mnmul.ikpmd_applicatie.Models.Grade;
import com.example.mnmul.ikpmd_applicatie.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class GradeProgressFragment extends Fragment {

    private FirebaseAuth mAuth;
    private GradeDataMapper gradeDataMapper;
    private SubjectDataMapper subjectDataMapper;
    private List<Grade> grades = new ArrayList<>();
    private int year;

    public GradeProgressFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        subjectDataMapper = new SubjectDataMapper(database);
        gradeDataMapper = new GradeDataMapper(database, subjectDataMapper);
        year = getActivity().getIntent().getIntExtra("selected_year", -1);
        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getUid() != null) {
            fetchGrades();

            if (year > 2) {
                fetchGradesPreviousYear();
            }
        }

        return inflater.inflate(R.layout.grade_progress_fragment, container, false);
    }

    public void handleUIChanges() {
        setRequiredPoints();
        setAverageGrade();
        setAchievedEC();
        setAchievedPointsPercentage();
    }

    public void fetchGrades() {
        gradeDataMapper.getAll(mAuth.getUid(), year, new Listener<Grade>() {
            @Override
            public void onObjectAdded(Grade grade) {
                grades.add(grade);
                handleUIChanges();
            }

            @Override
            public void onObjectChanged(Grade grade) {
                grades.set(grades.indexOf(grade), grade);
                handleUIChanges();
            }

            @Override
            public void onObjectDeleted(Grade grade) {
                grades.remove(grade);
                handleUIChanges();
            }
        });
    }

    public void fetchGradesPreviousYear() {
        gradeDataMapper.getAll(mAuth.getUid(), 2, new Listener<Grade>() {
            @Override
            public void onObjectAdded(Grade grade) {
                grades.add(grade);
                handleUIChanges();
            }

            @Override
            public void onObjectChanged(Grade grade) {
                grades.set(grades.indexOf(grade), grade);
                handleUIChanges();
            }

            @Override
            public void onObjectDeleted(Grade grade) {
                grades.remove(grade);
                handleUIChanges();
            }
        });
    }

    public void setAverageGrade() {
        TextView averageScoreText = getView().findViewById(R.id.text_average_grade);
        ProgressBar scoreProgressBar = getView().findViewById(R.id.progress_grade);
        double averageGrade = GradeHelper.getAverageGrade(grades);

        int colorGreen = ContextCompat.getColor(this.getContext(), R.color.colorAccent);
        int colorRed = ContextCompat.getColor(this.getContext(), R.color.colorPrimary2);
        int colorYellow = ContextCompat.getColor(this.getContext(), R.color.colorPrimary3);
        int gradeColor = colorGreen;

        if (averageGrade < 5.5) {
            gradeColor = colorRed;
        } else if (averageGrade < 7.5) {
            gradeColor = colorYellow;
        }

        averageScoreText.setText(String.valueOf(averageGrade));
        scoreProgressBar.getProgressDrawable().setColorFilter(gradeColor, PorterDuff.Mode.SRC_IN);
    }

    public void setAchievedEC() {
        TextView totalPointsText = getView().findViewById(R.id.text_total_points);
        int achievedPoints = GradeHelper.getAchievedPoints(grades);
        totalPointsText.setText(String.valueOf(achievedPoints));
    }

    public void setAchievedPointsPercentage() {
        ProgressBar pointsProgressBar = getView().findViewById(R.id.progress_points);
        int achievedPoints = GradeHelper.getAchievedPoints(grades);
        int totalEC = getRequiredPoints();
        double percentage = (achievedPoints / (double)totalEC) * 100;
        int roundedPercentage = (int)Math.round(percentage);
        pointsProgressBar.setProgress(roundedPercentage);
    }

    public int getRequiredPoints() {
        if (year == 1) {
            return Settings.REQUIRED_EC_PROPEDEUSE;
        } else {
            return Settings.REQUIRED_EC_BACHELOR;
        }
    }

    public void setRequiredPoints() {
        TextView requiredPointsText = getView().findViewById(R.id.text_required_points);
        int requiredPoints = getRequiredPoints();
        requiredPointsText.setText("/ " + requiredPoints);
    }

}