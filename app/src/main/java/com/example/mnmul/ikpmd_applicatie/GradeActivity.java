package com.example.mnmul.ikpmd_applicatie;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.example.mnmul.ikpmd_applicatie.Adapters.SubjectArrayAdapter;
import com.example.mnmul.ikpmd_applicatie.Config.Messages;
import com.example.mnmul.ikpmd_applicatie.DataMappers.GradeDataMapper;
import com.example.mnmul.ikpmd_applicatie.DataMappers.SubjectDataMapper;
import com.example.mnmul.ikpmd_applicatie.Listeners.Listener;
import com.example.mnmul.ikpmd_applicatie.Models.Grade;
import com.example.mnmul.ikpmd_applicatie.Models.Subject;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class GradeActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    private int year;
    private Grade grade;
    private String gradeId;
    private String score;
    private Subject selectedSubject;
    private GradeDataMapper gradeDataMapper;
    private SubjectDataMapper subjectDataMapper;
    private List<Subject> subjects = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);
        getActivityIntent();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        subjectDataMapper = new SubjectDataMapper(database);
        gradeDataMapper = new GradeDataMapper(database, subjectDataMapper);
        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getUid() != null) {
            fetchGrade();
        }

        fetchSubjects();
        setUpUI();
    }

    public void setUpUI() {
        setTitle();
        setBackToYearButtonTitle();
        createScoreInput();
        handleBackToYearButton();
        showDeleteButton();
        handleMarkGradeCheckboxes();
    }

    public void handleUIChanges() {
        setCheckboxValues();
        createScoreInput();
        createSubjectDropdown();
        selectGradeSubjectInDropdown();
    }

    public void fetchSubjects() {
        subjectDataMapper.getAll(year, new Listener<Subject>() {
            @Override
            public void onObjectAdded(Subject object) {
                subjects.add(object);
                handleUIChanges();
            }

            @Override
            public void onObjectChanged(Subject object) {
                subjects.remove(object);
                subjects.add(object);
                handleUIChanges();
            }

            @Override
            public void onObjectDeleted(Subject object) {
                subjects.remove(object);
                handleUIChanges();
            }
        });
    }

    public void fetchGrade() {
        gradeDataMapper.find(mAuth.getUid(), gradeId, new Listener<Grade>() {
            @Override
            public void onObjectAdded(Grade object) {
                grade = object;
                score = object.getScore();
                selectedSubject = object.getSubject();
                handleUIChanges();
            }

            @Override
            public void onObjectChanged(Grade object) {
                grade = object;
                score = object.getScore();
                selectedSubject = object.getSubject();
                handleUIChanges();
            }

            @Override
            public void onObjectDeleted(Grade object) {
                grade = null;
                score = "";
                selectedSubject = null;
            }
        });
    }

    public void getActivityIntent() {
        year = getIntent().getIntExtra("selected_year", -1);
        gradeId = getIntent().getStringExtra("selected_grade_id");
    }

    public void setTitle() {
        TextView title = findViewById(R.id.text_title);

        if (!gradeId.equals("")) {
            title.setText(getResources().getString(R.string.edit_grade));
        }
    }

    public void setCheckboxValues() {
        CheckBox sufficientCheckBox = findViewById(R.id.checkbox_mark_sufficient);
        CheckBox insufficientCheckBox = findViewById(R.id.checkbox_mark_insufficient);

        if (score != null && score.equals("V")) {
            sufficientCheckBox.setChecked(true);
        }

        if (score != null && score.equals("O")) {
            insufficientCheckBox.setChecked(true);
        }
    }

    public void handleMarkGradeCheckboxes() {
        CheckBox sufficientCheckBox = findViewById(R.id.checkbox_mark_sufficient);
        CheckBox insufficientCheckBox = findViewById(R.id.checkbox_mark_insufficient);
        sufficientCheckBox.setTypeface(ResourcesCompat.getFont(this.getApplicationContext(), R.font.montserrat));
        insufficientCheckBox.setTypeface(ResourcesCompat.getFont(this.getApplicationContext(), R.font.montserrat));

        sufficientCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                score = "V";
                insufficientCheckBox.setChecked(false);
                disableScoreInput();
            } else {
                if (!insufficientCheckBox.isChecked()) {
                    enableScoreInput();
                }
            }
        });

        insufficientCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                score = "O";
                sufficientCheckBox.setChecked(false);
                disableScoreInput();
            } else {
                if (!sufficientCheckBox.isChecked()) {
                    enableScoreInput();
                }
            }
        });
    }

    public void submitForm(View view) {
        EditText scoreInput = findViewById(R.id.input_score);
        String score = scoreInput.getText().toString();

        if (checkFormInput()) {
            if (grade != null) {
                editGrade(grade, selectedSubject, score);
            } else {
                addGrade(selectedSubject, score);
            }
        }
    }

    public void disableScoreInput() {
        EditText scoreInput = findViewById(R.id.input_score);
        scoreInput.setFocusableInTouchMode(false);
        scoreInput.setText(score);
        scoreInput.clearFocus();
        scoreInput.setTextColor(ContextCompat.getColor(this.getApplicationContext(), R.color.textColorLight));
    }

    public void enableScoreInput() {
        EditText scoreInput = findViewById(R.id.input_score);
        scoreInput.setFocusableInTouchMode(true);
        scoreInput.setText("");
        scoreInput.setTextColor(ContextCompat.getColor(this.getApplicationContext(), R.color.textColorPrimary));
    }

    public boolean checkFormInput() {
        EditText scoreInput = findViewById(R.id.input_score);
        String score = scoreInput.getText().toString();
        boolean scoreValid;
        boolean selectedSubjectValid = selectedSubject != null;

        try {
            scoreValid = checkScoreInput(Double.parseDouble(score));
        } catch(NumberFormatException e) {
            scoreValid = (score.equals("V") || score.equals("O"));
        }

        if (!scoreValid) {
            scoreInput.setTextColor(ContextCompat.getColorStateList(this.getApplicationContext(), R.color.colorPrimary2));
        }

        return scoreValid && selectedSubjectValid;
    }

    public boolean checkScoreInput(double score) {
        return score >= 0.0 && score <= 10.0;
    }

    public void addGrade(Subject subject, String score) {
        Grade grade = new Grade(year, subject.getKey(), score);

        gradeDataMapper.insert(grade, mAuth.getUid(), object -> {
            Toast toast = Toast.makeText(
                    GradeActivity.this,
                    Messages.getGradeAddedSuccessMessage(score, subject.getCode()),
                    Toast.LENGTH_LONG
            );
            toast.show();
        }, exception -> {
            Log.e("Inserting object failed", "" + exception.getMessage());
            Toast t = Toast.makeText(GradeActivity.this, Messages.getGradeAddedFailureMessage(), Toast.LENGTH_LONG);
            t.show();
        });

        startYearActivity(year);
    }

    public void editGrade(Grade grade, Subject subject, String score) {
        grade.setScore(score);
        grade.setSubjectId(subject.getKey());

        gradeDataMapper.update(grade, mAuth.getUid(), object -> {
            Toast toast = Toast.makeText(
                    GradeActivity.this,
                    Messages.getGradeEditedSuccessMessage(subject.getCode()),
                    Toast.LENGTH_LONG
            );
            toast.show();
        }, exception -> {
            Log.e("Editing object failed", "" + exception.getMessage());
            Toast t = Toast.makeText(GradeActivity.this, Messages.getGradeEditedFailureMessage(), Toast.LENGTH_LONG);
            t.show();
        });

        startYearActivity(year);
    }

    public void deleteGrade(View view) {
        gradeDataMapper.delete(grade, mAuth.getUid(), object -> {
            Toast toast = Toast.makeText(GradeActivity.this, Messages.getGradeDeletedSuccessMessage(), Toast.LENGTH_LONG);
            toast.show();
        }, exception -> {
            Log.e("Deleting object failed", "" + exception.getMessage());
            Toast t = Toast.makeText(GradeActivity.this, Messages.getGradeDeletedFailureMessage(), Toast.LENGTH_LONG);
            t.show();
        });

        startYearActivity(year);
    }

    public void showDeleteButton() {
        if (!gradeId.equals("")) {
            Button deleteButton = findViewById(R.id.button_delete_grade);
            deleteButton.setVisibility(View.VISIBLE);
        }
    }

    public void setBackToYearButtonTitle() {
        Button backToYearButton = findViewById(R.id.button_back_to_year);
        String title = year == 3 ? ("Jaar " + year + "/" + (year + 1)) : ("Jaar " + year);
        backToYearButton.setText(title);
    }

    public void handleBackToYearButton() {
        Button backToYearButton = findViewById(R.id.button_back_to_year);
        backToYearButton.setOnClickListener(view -> startYearActivity(year));
    }

    public void startYearActivity(int year) {
        Intent intent = new Intent(GradeActivity.this, YearActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("selected_year", year);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    public void selectGradeSubjectInDropdown() {
        Spinner spinner = findViewById(R.id.subjects_spinner);

        if (selectedSubject != null && subjects != null) {
            spinner.setSelection(subjects.indexOf(selectedSubject));
        }
    }

    public void createSubjectDropdown() {
        Spinner spinner = findViewById(R.id.subjects_spinner);
        ArrayAdapter<Subject> adapter = new SubjectArrayAdapter(GradeActivity.this, 0, subjects);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                selectedSubject = adapter.getItem(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    public void createScoreInput() {
        EditText editText = findViewById(R.id.input_score);

        if (grade != null) {
            editText.setText(grade.getScore());
        }
    }

}