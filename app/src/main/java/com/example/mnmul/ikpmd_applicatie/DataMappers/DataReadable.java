package com.example.mnmul.ikpmd_applicatie.DataMappers;

import com.example.mnmul.ikpmd_applicatie.Listeners.Listener;
import com.google.firebase.database.DatabaseReference;

public interface DataReadable<Key, Type> {

    DatabaseReference getReference();

    void find(Key id, Listener<Type> listener);

    void getAll(int year, Listener<Type> listener);

}