package com.example.mnmul.ikpmd_applicatie.Listeners;

public interface Listener<Type> {

    void onObjectAdded(Type object);
    void onObjectChanged(Type object);
    void onObjectDeleted(Type object);

}